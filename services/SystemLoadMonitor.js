var schedule = require('node-schedule');
var os  = require('os-utils');

var start = function() {
  var persistenceTime = sails.config.application.persistencePeriodInSeconds;
  var cronExpression = '*/' + persistenceTime + ' * * * * *'
  schedule.scheduleJob(cronExpression, processLoadInfo);
}

var processLoadInfo = function() {
   removeOldStatistics();
   persistSystemLoad();
}

var removeOldStatistics = function() {
  SystemLoad.destroy({ createdAt: { '<': yesterday() } })
}

var persistSystemLoad = function() {
  os.cpuUsage(function(value) {
    var totalMemoryPercentage = 100;
    var decimals = 2;
    var memory = (totalMemoryPercentage - os.freememPercentage()).toFixed(decimals);
    sails.log("Memory used: " + memory + " %");
    var cpu = value.toFixed(decimals);
    sails.log("CPU used: " + cpu + " %");
    SystemLoad.create({ cpu: cpu, memory: memory }, logError);
  });
}

var logError = function(err, model) {
  if(err) {
    sails.log.error('Error occured', err);
  }
}

var yesterday = function() {
  var numberOfDays = 1;
  var date = new Date();
  date.setDate(date.getDate() - numberOfDays)
  return date;
}

module.exports.start = start
