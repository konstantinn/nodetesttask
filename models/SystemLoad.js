module.exports = {
  attributes: {
    cpu: {
      type: 'float',
      required: true,
      float: true
    },
    memory: {
      type: 'float',
      required: true,
      float: true
    }
  },
  renderSummery: function(res) {
    var query = "SELECT MAX(cpu) as cpu_max, MIN(cpu) as cpu_min, ROUND(AVG(cpu), 2) as cpu_avg, MAX(memory) as memory_max, MIN(memory) as memory_min, ROUND(AVG(memory), 2) as memory_avg, CONCAT(DATE_FORMAT(createdAt, '%Y-%m-%d %H:00:00'), '|', DATE_FORMAT(createdAt, '%Y-%m-%d %H:59:59')) as time_interval FROM systemload GROUP BY EXTRACT(DAY_HOUR FROM createdAt);"
    SystemLoad.query(query, function(err, systemLoad) {
      var statsHash =  systemLoad.map(function(stats) {
        return {
          interval: stats['time_interval'].split('|'),
          cpu: {
            min: stats['cpu_min'],
            max: stats['cpu_max']
          },
          memory: {
            min: stats['memory_min'],
            max: stats['memory_max']
          }
        }
      });
      res.json(statsHash)
    })
  }
};
